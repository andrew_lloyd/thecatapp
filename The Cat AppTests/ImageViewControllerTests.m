//
//  ImageViewControllerTests.m
//  The Cat App
//
//  Created by Andrew Lloyd on 15/10/2015.
//  Copyright © 2015 alloyd. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ImageViewController.h"

@interface ImageViewControllerTests : XCTestCase


@property ImageViewController *viewController;

@end

@implementation ImageViewControllerTests

- (void)setUp {
    [super setUp];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    self.viewController = (ImageViewController *)[storyboard instantiateViewControllerWithIdentifier:@"Image Viewer"];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testScrollViewContentSizeIsSetToImageSizeWhenImageIsSet
{
    UIImage *image = [UIImage imageNamed:@"testImage.jpg"];
    self.viewController.image = image;
    
    [self.viewController view];
    
    CGSize expectedSize = image.size;
    XCTAssertEqual(self.viewController.scrollView.contentSize.height, expectedSize.height);
    XCTAssertEqual(self.viewController.scrollView.contentSize.width, expectedSize.width);
}

@end
