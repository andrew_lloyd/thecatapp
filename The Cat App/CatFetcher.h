//
//  CatFetcher.h
//  The Cat App
//
//  Created by Andrew Lloyd on 13/10/2015.
//  Copyright © 2015 alloyd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CatFetcher : NSObject

- (void)downloadACatwithSuccess:(void (^) (UIImage *image))success
                     andFailure:(void (^) (NSError *error))failure;

@end
