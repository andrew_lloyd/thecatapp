//
//  AppDelegate.h
//  The Cat App
//
//  Created by Andrew Lloyd on 13/10/2015.
//  Copyright © 2015 alloyd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

