//
//  HomeCatViewController.m
//  The Cat App
//
//  Created by Andrew Lloyd on 13/10/2015.
//  Copyright © 2015 alloyd. All rights reserved.
//

#import "HomeCatViewController.h"
#import "ImageViewController.h"

@interface HomeCatViewController ()

@property (nonatomic)  BOOL downloading;

@end

@implementation HomeCatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"The Cat App";
    self.loadingIndicator.hidesWhenStopped = YES;
}

- (void)setDownloading:(BOOL)downloading
{
        _downloading = downloading;
        if (downloading)
        {
            
            self.fetchCatButton.backgroundColor = [UIColor lightGrayColor];
        }
        else
        {
            self.fetchCatButton.backgroundColor = [UIColor redColor];
        }
}

- (CatFetcher*)catFetcher
{
    if (_catFetcher == nil)
    {
        _catFetcher = [[CatFetcher alloc] init];
    }
    return _catFetcher;
}

- (void)fetchACat
{
    if (!self.downloading)
    {
        self.downloading = YES;
        [self.catFetcher downloadACatwithSuccess:^(UIImage *image)
         {
                 [self.loadingIndicator stopAnimating];
                 self.downloading = NO;
             
             [self.imageButton setBackgroundImage:image forState:UIControlStateNormal];
             self.catImage = image;
         }
                                      andFailure:^(NSError *error) {
                                          UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Looks like we've had kitty problems!"
                                                                                                         message:error.localizedDescription
                                                                                                  preferredStyle:UIAlertControllerStyleAlert];
                                          
                                          UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                                                handler:^(UIAlertAction * action) {
                                                                                                        [self.loadingIndicator stopAnimating];
                                                                                                        self.downloading = NO;
                                                                                                }];
                                          
                                          [alert addAction:defaultAction];
                                          [self presentViewController:alert animated:YES completion:nil];
                                      }];
    }
}

- (IBAction)getCatButtonPressed:(id)sender
{
    [self.loadingIndicator startAnimating];
    [self fetchACat];
}

- (IBAction)mealImageButtonPressed:(id)sender
{
    if (self.catImage)
    {
        ImageViewController *ivc = [self.storyboard instantiateViewControllerWithIdentifier:@"Image Viewer"];
        ivc.image = self.catImage;
        
        [self.navigationController pushViewController:ivc animated:YES];
    }
}

@end
