//
//  HomeCatViewController.h
//  The Cat App
//
//  Created by Andrew Lloyd on 13/10/2015.
//  Copyright © 2015 alloyd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CatFetcher.h"

@interface HomeCatViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *fetchCatButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;
@property (weak, nonatomic) IBOutlet UIButton *imageButton;

@property (weak, nonatomic) UIImage *catImage;
@property (nonatomic, strong) CatFetcher *catFetcher;

- (IBAction)getCatButtonPressed:(id)sender;
- (IBAction)mealImageButtonPressed:(id)sender;

@end
