//
//  CatFetcher.m
//  The Cat App
//
//  Created by Andrew Lloyd on 13/10/2015.
//  Copyright © 2015 alloyd. All rights reserved.
//

#import "CatFetcher.h"

@interface CatFetcher ()

@end

@implementation CatFetcher

- (void)downloadACatwithSuccess:(void (^) (UIImage *image))success
                     andFailure:(void (^) (NSError *error))failure
{
    NSURL *url = [NSURL URLWithString:@"http://thecatapi.com/api/images/get"];

    NSURLSessionDownloadTask *downloadImageTask = [[NSURLSession sharedSession]
                                          downloadTaskWithURL:url completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
                                              if (error)
                                              {
                                                  failure(error);
                                              }
                                              else
                                              {
                                                  UIImage *downloadedImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:location]];
                                                  success(downloadedImage);
                                              }
                                          }];
    
    [downloadImageTask resume];
}


@end
