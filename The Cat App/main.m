//
//  main.m
//  The Cat App
//
//  Created by Andrew Lloyd on 13/10/2015.
//  Copyright © 2015 alloyd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
