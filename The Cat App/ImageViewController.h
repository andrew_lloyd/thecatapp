//
//  ImageViewController.h
//  The Cat App
//
//  Created by Andrew Lloyd on 15/10/2015.
//  Copyright © 2015 alloyd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic, weak) UIImage *image;

@end
