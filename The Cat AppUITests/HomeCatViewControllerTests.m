//
//  HomeCatViewControllerTests.m
//  The Cat App
//
//  Created by Andrew Lloyd on 14/10/2015.
//  Copyright © 2015 alloyd. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <UIKit/UIKit.h>
#import "HomeCatViewController.h"
#import "CatFetcher.h"
#import "OCMock.h"

@interface HomeCatViewControllerTests : XCTestCase

@property HomeCatViewController *viewController;

@end

@implementation HomeCatViewControllerTests

- (void)setUp
{
    [super setUp];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    self.viewController = (HomeCatViewController *)[storyboard instantiateViewControllerWithIdentifier:@"Home"];
}

- (void)tearDown
{
    [super tearDown];
}

- (void)testViewControllerSetup
{
    [_viewController view];
    
    XCTAssertNotNil(self.viewController);
}

- (void)testLoadingIndicatorIsAnimatingOnceDownloadButtonIsPressed
{
    [self.viewController view];
    
    XCTAssertFalse([self.viewController.loadingIndicator isAnimating]);
    
    UIButton *sampleButton = [[UIButton alloc] init];
    [sampleButton setTitle:@"sample download button" forState:UIControlStateNormal];
    
    [self.viewController getCatButtonPressed:sampleButton];
    
    XCTAssertTrue([self.viewController.loadingIndicator isAnimating]);
    XCTAssertFalse([self.viewController.loadingIndicator isHidden]);
}

- (void)testImageIsSetOnSuccesfulCall
{
    UIImage *testImage = [UIImage imageNamed:@"testImage.jpg"];
    
    id mockService = [self mockServiceReturningSuccess];
    
    [self.viewController view];
    self.viewController.catFetcher = mockService;
    
    XCTAssertNil(self.viewController.catImage);
 
    UIButton *sampleButton = [[UIButton alloc] init];
    [sampleButton setTitle:@"sample download button" forState:UIControlStateNormal];
    
    [self.viewController getCatButtonPressed:sampleButton];
    
    XCTAssertEqualObjects(self.viewController.catImage, testImage);
    
}

- (void)testFailureCall
{
    id mockService = [self mockServiceReturningFailure];
    
    [self.viewController view];
    self.viewController.catFetcher = mockService;
    
    XCTAssertNil(self.viewController.catImage);
    
    UIButton *sampleButton = [[UIButton alloc] init];
    [sampleButton setTitle:@"sample download button" forState:UIControlStateNormal];
    
    [self.viewController getCatButtonPressed:sampleButton];
    
    XCTAssertNil(self.viewController.catImage);
    
    //This isn't working, need to find out how to test alert is displayed
    XCTAssertNotNil(self.viewController.presentedViewController);
}

- (id)mockServiceReturningSuccess
{
    id mockService = OCMClassMock([CatFetcher class]);
    OCMStub([mockService downloadACatwithSuccess:[OCMArg any]
                                      andFailure:[OCMArg any]]).andDo(^(NSInvocation *invocation)
                                                                                             {
                                                                                                 void(^success)(UIImage *image) = nil;
                                                                                                 [invocation getArgument:&success atIndex:2];
                                                                                                 success([UIImage imageNamed:@"testImage.jpg"]);
                                                                                             });
    
    return mockService;
}

- (id)mockServiceReturningFailure
{
    id mockService = OCMClassMock([CatFetcher class]);
    OCMStub([mockService downloadACatwithSuccess:[OCMArg any]
                                      andFailure:[OCMArg any]]).andDo(^(NSInvocation *invocation)
                                                                      {
                                                                          void(^failure)(NSError *error) = nil;
                                                                          [invocation getArgument:&failure atIndex:3];
                                                                          failure([NSError errorWithDomain:@"com.cats.fatalError" code:200 userInfo:nil]);
                                                                      });
    
    return mockService;
}

@end
